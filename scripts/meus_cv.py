import numpy as np
import pandas as pd
from catboost import CatBoostClassifier
from sklearn.model_selection import StratifiedKFold, ParameterGrid
from sklearn.metrics import accuracy_score
from tqdm import tqdm

RANDOM_STATE = 25


def catboost_cv(X: pd.DataFrame, y: pd.DataFrame, X_test: pd.DataFrame, params: dict, cat_features: list, n_splits: int = 5, random_state: int = RANDOM_STATE):
    stratified_fold = StratifiedKFold(
        n_splits=n_splits, shuffle=True, random_state=random_state)
    accuracy_result = []

    for train_idx, validation_idx in stratified_fold.split(X, y):
        # Train set
        X_train = X[train_idx]
        y_train = y[train_idx]

        # Validation set (K)
        X_validation = X[validation_idx]
        y_validation = y[validation_idx]

        classifier = CatBoostClassifier(
            iterations=params['iterations'],
            loss_function=params['loss_function'],
            custom_loss=params['custom_loss'],
            learning_rate=params['learning_rate'],
            use_best_model=True,
            od_type='Iter',
            od_wait=10,
            task_type='GPU'
        )

        # Do training
        classifier.fit(
            X_train,
            y_train,
            cat_features=cat_features,
            eval_set=(X_validation, y_validation)
        )
        # Get metric
        y_prediction = classifier.predict(X_validation)
        accuracy = accuracy_score(y_validation, y_prediction)
        accuracy_result.append(accuracy)

    return (sum(accuracy_result) / n_splits)


def catboost_gridsearch(X: pd.DataFrame, y: pd.DataFrame, X_test: pd.DataFrame, params: dict, cat_features: list, n_splits=5):
    params_bag = {
        'accuracy': 0,
        'parameters': []
    }
    prediction = None

    for parameters in tqdm(list(ParameterGrid(params)), ascii=True, desc='Parameters tunning:'):
        accuracy = catboost_cv(X, y, X_test, parameters,
                               cat_features, n_splits=n_splits)
        if accuracy_score > params_bag['accuracy']:
            params_bag['accuracy'] = accuracy_score
            params_bag['parameters'] = parameters

        print('Accuracy: {}\nParameters: {}'.format(
            str(accuracy_score), str(parameters)))

    print('\nBest Result:\nAccuracy: {}\nParameters: {}'.format(
        str(accuracy_score), str(parameters)))
    return params_bag


params = {
    'loss_function': 'Logloss',
    'iterations': 2000,
    'custom_loss': 'AUC',
    'learning_rate': 0.3
}
