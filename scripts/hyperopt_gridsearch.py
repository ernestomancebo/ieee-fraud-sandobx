import pandas as pd
import hyperopt
import numpy as np
import catboost as cb


def FraudClassifierObjective(object):
    def __init__(self, dataset, const_params, fold_count):
        self._dataset = dataset
        self._const_params = const_params.copy()
        self._fold_count = fold_count
        self._evaluated_count = 0

    def _to_catboost_params(self, hyper_params):
        return {
            'learning_rate': hyper_params['learning_rate'],
            'depth': hyper_params['depth'],
            'l2_leaf_reg': hyper_params['l2_leaf_reg']
        }

    def __call__(self, hyper_params):
        params = self._to_catboost_params(hyper_params)
        params.update(self._const_params)

        print('Evaluating parms: {}'.format(params))

        shuffle_split = ShuffleSplit(
            n_splits=self._fold_count, test_size=.25, random_state=25)
        scores = cb.cv(
            pool=self._dataset,
            params=params,
            folds=stratified_shuffle_split,
            partition_random_seed=25,
            early_stopping_rounds=10,
            verbose=False
        )

        max_mean_auc = np.max(scores['test-AUC-mean'])
        print('evaluated score={}'.format(max_mean_auc))

        self._evaluated_count += 1
        print('Evaluated {} times'.format(self._evaluated_count))

        return {
            'loss': -max_mean_aux,
            'status': hyperopt.STATUS_OK
        }


def find_best_hyper_params(dataset, const_params, max_evals=100):
    parameter_space = {
        'learning_rate': hyperopt.hp.uniform('learning_rate', 0.01, 0.8),
        'depth': hyperopt.hp.randint('depth', 12),
        'l2_leaf_reg': hyperopt.hp.uniform('l2_leaf_reg', 1, 10)
    }

    objective = FraudClassifierObjective(
        dataset=dataset, const_params=const_params, fold_count=6)
    trials = hyperopt.Trials()
    best = hyperopt.fmin(
        fn=objective,
        space=parameter_space,
        algo=hyperopt.rand.suggest,
        max_evals=max_evals,
        rstate=np.random.RandomState(seed=25)
    )

    return best


def train_best_model(X, y, const_params, max_evals=100, use_default=False):
    dataset = cb.Pool(X, y, cat_features=np.where(X.dtypes == np.object)[0])

    # Switch in a future
    if use_default:
        # pretrained optimal parameters
        best = {
            'learning_rate': 0.4234185321620083,
            'depth': 5,
            'l2_leaf_reg': 9.464266235679002
        }
    else:
        best = find_best_hyper_params(
            dataset, const_params, max_evals=max_evals)

    hyper_params = best.copy()
    hyper_params.update(const_params)
    hyper_params.pop('use_best_model', None)

    model = cb.CatBoostClassifier(**hyper_params)
    model.fit(dataset, verbose=True)

    return model, hyper_params


# Training and bs
have_gpu = True
use_optimal_parameters = False
hyperopt_iterations = 15

const_params = dict({
    'task_type': 'GPU' if have_gpu else 'CPU',
    'loss_function': 'Logloss',
    'eval_metric': 'AUC',
    'custom_metric': ['AUC'],
    'iterations': 100,
    'random_seed': 25
})

model, params = train_best_model(
    X_train,
    y_train,
    const_params,
    max_evals=hyperopt_iterations,
    use_default=use_optimal_parameters
)

print('Best params are {}', format(params))
