import ast
import json
import os

import numpy as np
import pandas as pd


def get_best(for_model: str, top: int = 3, tail: int = None):
    ROOT_PATH = f'./model/angora_boost/{for_model}/'
    files = os.listdir(ROOT_PATH)
    result_list = []

    if tail is None:
        tail = top

    consolidated = pd.DataFrame()
    for file_name in files:
        if not file_name.endswith('.csv') or not file_name.startswith('optimization'):
            continue

        result = pd.read_csv(f'{ROOT_PATH}{file_name}')
        # Only consider those files with results
        if result.shape[0] == 0:
            # Since those files doesn't apport nothig, we delete them to improve
            # future searchs.
            os.remove(f'{ROOT_PATH}{file_name}')
            continue

        consolidated.append(result)

    sort_result_df(result)
    best, worst = to_json_result(result, top, tail)

    with open(f'{ROOT_PATH}best_{for_model}.json', 'w') as write_file:
        json.dump(best, write_file, indent=4)

    with open(f'{ROOT_PATH}worst_{for_model}.json', 'w') as write_file:
        json.dump(worst, write_file, indent=4)

    print(f'{for_model} - completed')


def to_json_result(result: pd.DataFrame, top: int, tail: int):
    best = []
    worst = []

    for idx in range(0, top):
        result_dict = {
            'loss': result.loc[idx, 'loss'],
            'params': ast.literal_eval(result.loc[idx, 'params'])
        }
        best.append(result_dict)

    limit = len(result)
    for idx in range(limit - tail, limit):
        result_dict = {
            'loss': result.loc[idx, 'loss'],
            'params': ast.literal_eval(result.loc[idx, 'params'])
        }
        worst.append(result_dict)

    return best, worst


def sort_result_df(result: pd.DataFrame):
    result.sort_values('loss', ascending=True, inplace=True)
    result.reset_index(inplace=True, drop=True)


get_best('catboost', top=5)
get_best('random_forest')
get_best('lgbm')
