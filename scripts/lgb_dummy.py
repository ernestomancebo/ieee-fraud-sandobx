import lightgbm as lgb
import numpy as np
import pandas as pd
from hyperopt import hp

d_train = lgb.Dataset(X_train, y_train)

space = {
    'num_leaves': hp.quniform('num_leaves', 8, 128, 2),
    'colsample_bytree': hp.uniform('colsample_bytree', 0.3, 1.0),
    'learning_rate': hp.choice('learning_rate', 0.003, 0.01),
}

params = {
    # 'learning_rate' = 0.003,
    'boosting_type': 'gbdt',
    'objective': 'binary',
    'metric': 'binary_logloss',
    'sub_feature': 0.5,
    'min_data': 50,
    'max_depth': 10,
    'num_stimators': 1000,
    'metric': 'auc'
}

clf = lgb.LGBMClassifier(**params)


class BayesianLgbClassifierObjective(object):
    def __init__(self, dataset, const_params, fold_count, out_file):
        self._dataset = dataset
        self._const_params = const_params.copy()
        self._fold_count = fold_count
        self._evaluated_count = 0
        self._out_file = out_file

    def _to_lgb_params(self, hyper_params):
        """Search Space parameters"""

        return {
            'num_leaves': hyper_params['num_leaves'],
            'colsample_bytree': hyper_params['colsample_bytree'],
            'learning_rate': hyper_params['learning_rate'],
        }

    def __call__(self, hyper_params):
        params = self._to_lgb_params(hyper_params)
        params.update(self._const_params)

        print('Evaluating parms: {}'.format(params))

        stratified_shuffle_split = StratifiedShuffleSplit(
            n_splits=self._fold_count, test_size=0.3, random_state=25)
        start = timer()
        scores = lgb.cv(
            params=params,
            folds=stratified_shuffle_split,
            train_set=self._dataset,
            early_stopping_rounds=10,
            verbose_eval=False
        )

        # scores = cb.cv(
        #     pool=self._dataset,
        #     params=params,
        #     folds=stratified_shuffle_split,
        #     partition_random_seed=25,
        #     early_stopping_rounds=10,
        #     verbose=False
        # )
        run_time = timer() - start

        max_mean_auc = np.max(scores['auc-mean'])
        loss = 1 - max_mean_auc

        # Iteración de Cross Validation con mejor resultado
        n_stimators = int(np.argmax(scores['auc-mean']) + 1)
        self._evaluated_count += 1

        file_connection = open(self._out_file, 'a', newline='')
        writer = csv.writer(file_connection)
        writer.writerow(
            [loss, params, self._evaluated_count, n_stimators, run_time])
        file_connection.close()

        print('evaluated score={}'.format(max_mean_auc))
        print('Evaluated {} times'.format(self._evaluated_count))

        return {
            'loss': loss,
            'params': params,
            'evaluated_count': self._evaluated_count,
            'n_stimators': n_stimators,
            'run_time': run_time,
            'status': hyperopt.STATUS_OK
        }


def lgbm_find_best_hyper_params(dataset, const_params, out_file, max_evals=100):
    objective = BayesianLgbClassifierObjective(
        dataset=dataset, const_params=const_params, fold_count=6, out_file=out_file)
    domain_space = domain_space = {
        'num_leaves': hp.quniform('num_leaves', 8, 128, 2),
        'colsample_bytree': hp.uniform('colsample_bytree', 0.3, 1.0),
        'learning_rate': hp.choice('learning_rate', [0.003, 0.01]),
    }

    best = hyperopt.fmin(
        fn=objective,
        space=domain_space,
        algo=tpe.suggest,
        max_evals=max_evals,
        trials=bayes_lgbm_trials,
        rstate=np.random.RandomState(seed=25),
        return_argmin=False
    )

    return best


def lgbm_train_best_model(X, y, const_params, out_file, max_evals=100, use_default=False):
    d_train = lgb.Dataset(x.drop(['TransactionID'], axis=1),
                          y, categorical_feature=np.where(X.dtypes == 'object')[0])

    if use_default:
        # pretrained optimal parameters
        best = {
            'colsample_bytree': 0.5553731290544175,
            'learning_rate': 0.01,
            'num_leaves': 68.0,
            'boosting_type': 'gbdt',
            'objective': 'binary',
            'metric': 'auc',
            'sub_feature': 0.5,
            'min_data': 50,
            'max_depth': 10,
            'num_stimators': 1000
        }
    else:
        best = lgbm_find_best_hyper_params(
            d_train, const_params, out_file=out_file, max_evals=max_evals)

    hyper_params = best.copy()
    hyper_params.update(const_params)
    hyper_params.pop('use_best_model', None)

    model = lgb.train(hyper_params, d_train, hyper_params['iterations'])
    model.fit(dataset, verbose=True)

    return model, hyper_params
